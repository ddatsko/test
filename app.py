from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('main.html')


@app.route('/get_items')
def get_items():
    return render_template('db_items.html'
                           '')


if __name__ == '__main__':
    app.run(port=80, host='127.0.0.1')
